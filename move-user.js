(function ($) {

  Drupal.behaviors.moveUserAdmin = {
    attach: function (context, settings) {
      $('#edit-user-cancel-method input').once('move-user-admin', function () {
        $(this).change(function (e) {
          if ($(e.target).val() == 'move_user_reassign') {
            $('.form-item-reassign-user').show();
          } else {
            $('.form-item-reassign-user').hide();
          }
        });
      });
      $('.form-item-reassign-user').hide();
    }
  };

})(jQuery);